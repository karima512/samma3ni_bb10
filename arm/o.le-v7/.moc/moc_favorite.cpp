/****************************************************************************
** Meta object code from reading C++ file 'favorite.hpp'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/favorite.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'favorite.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Favorite[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       5,   39, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   10,    9,    9, 0x05,
      52,   43,    9,    9, 0x05,
      91,   78,    9,    9, 0x05,
     129,  121,    9,    9, 0x05,
     162,  150,    9,    9, 0x05,

 // properties: name, type, flags
     195,  187, 0x0a495903,
     206,  187, 0x0a495903,
     216,  187, 0x0a495903,
     230,  187, 0x0a495903,
     235,  187, 0x0a495903,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,

       0        // eod
};

static const char qt_meta_stringdata_Favorite[] = {
    "Favorite\0\0newId\0favoriteIDChanged(QString)\0"
    "newRadio\0radioNameChanged(QString)\0"
    "newFrequence\0frequenceNameChanged(QString)\0"
    "newLogo\0logoChanged(QString)\0newUrlradio\0"
    "urlradioChanged(QString)\0QString\0"
    "favoriteID\0radioName\0frequenceName\0"
    "logo\0urlradio\0"
};

void Favorite::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Favorite *_t = static_cast<Favorite *>(_o);
        switch (_id) {
        case 0: _t->favoriteIDChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->radioNameChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->frequenceNameChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->logoChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->urlradioChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Favorite::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Favorite::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Favorite,
      qt_meta_data_Favorite, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Favorite::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Favorite::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Favorite::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Favorite))
        return static_cast<void*>(const_cast< Favorite*>(this));
    return QObject::qt_metacast(_clname);
}

int Favorite::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = favoriteID(); break;
        case 1: *reinterpret_cast< QString*>(_v) = radioName(); break;
        case 2: *reinterpret_cast< QString*>(_v) = frequenceName(); break;
        case 3: *reinterpret_cast< QString*>(_v) = logo(); break;
        case 4: *reinterpret_cast< QString*>(_v) = urlradio(); break;
        }
        _id -= 5;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setFavoriteID(*reinterpret_cast< QString*>(_v)); break;
        case 1: setRadioName(*reinterpret_cast< QString*>(_v)); break;
        case 2: setFrequenceName(*reinterpret_cast< QString*>(_v)); break;
        case 3: setLogo(*reinterpret_cast< QString*>(_v)); break;
        case 4: setUrlradio(*reinterpret_cast< QString*>(_v)); break;
        }
        _id -= 5;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Favorite::favoriteIDChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Favorite::radioNameChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Favorite::frequenceNameChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Favorite::logoChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Favorite::urlradioChanged(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
