<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>ArticleItem</name>
    <message>
        <location filename="../assets/ArticleItem.qml" line="62"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ErrorPage</name>
    <message>
        <location filename="../assets/ErrorPage.qml" line="13"/>
        <source>ERROR: Database Initialization Failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExternalIP</name>
    <message>
        <location filename="../src/ExternalIP.cpp" line="107"/>
        <source>Http Error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/ExternalIP.cpp" line="114"/>
        <source>Unable to retrieve ip address</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InfoPage</name>
    <message>
        <location filename="../assets/InfoPage.qml" line="13"/>
        <source>           About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/InfoPage.qml" line="15"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/InfoPage.qml" line="48"/>
        <source>Samma3ni is a radio hub for the gulf countries developed by :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/InfoPage.qml" line="75"/>
        <source>Email :</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayerPage</name>
    <message>
        <location filename="../assets/PlayerPage.qml" line="25"/>
        <source>Recorded Tracks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayerSheet</name>
    <message>
        <location filename="../assets/PlayerSheet.qml" line="33"/>
        <source>Recorded Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/PlayerSheet.qml" line="67"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgressBar</name>
    <message>
        <location filename="../assets/ProgressBar.qml" line="31"/>
        <source>%1:%2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationHandler</name>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="38"/>
        <source>Please wait while the application connects to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="135"/>
        <source>Application connected to BBM.  Press Continue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="147"/>
        <source>Disconnected by RIM. RIM is preventing this application from connecting to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="153"/>
        <source>Disconnected. Go to Settings -&gt; Security and Privacy -&gt; Application Permissions and connect this application to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="161"/>
        <source>Invalid UUID. Report this error to the vendor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="167"/>
        <source>Too many applications are connected to BBM. Uninstall one or more applications and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="175"/>
        <source>Cannot connect to BBM. Download this application from AppWorld to keep using it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="181"/>
        <source>Check your Internet connection and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="188"/>
        <source>Connecting to BBM. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="193"/>
        <source>Determining the status. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="203"/>
        <source>Would you like to connect the application to BBM?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>countries</name>
    <message>
        <location filename="../assets/countries.qml" line="25"/>
        <source>Choose category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/countries.qml" line="58"/>
        <source>Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/countries.qml" line="87"/>
        <source>Koran</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/countries.qml" line="115"/>
        <source>Various</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>favoris</name>
    <message>
        <location filename="../assets/favoris.qml" line="40"/>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/favoris.qml" line="49"/>
        <source>No entries in this view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/favoris.qml" line="158"/>
        <source>Long press an item to remove from list after open it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/favoris.qml" line="159"/>
        <location filename="../assets/favoris.qml" line="165"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="46"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="82"/>
        <source>BBM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>newscategories</name>
    <message>
        <location filename="../assets/newscategories.qml" line="34"/>
        <source>Radio News Feeds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/newscategories.qml" line="275"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>playRecordedTrack</name>
    <message>
        <location filename="../assets/playRecordedTrack.qml" line="23"/>
        <source>Play Recorded Tracks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>radios</name>
    <message>
        <location filename="../assets/radios.qml" line="41"/>
        <source>All stations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/radios.qml" line="69"/>
        <source>Loading data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/radios.qml" line="205"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>record</name>
    <message>
        <location filename="../assets/record.qml" line="32"/>
        <source>Recording</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>recordinglist</name>
    <message>
        <location filename="../assets/recordinglist.qml" line="24"/>
        <source>Recorded Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/recordinglist.qml" line="39"/>
        <source>No entries in this view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/recordinglist.qml" line="74"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/recordinglist.qml" line="75"/>
        <source>Delete all recorded tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/recordinglist.qml" line="79"/>
        <source>Clear All Tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/recordinglist.qml" line="118"/>
        <source>Delete recorded tracks </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/recordinglist.qml" line="119"/>
        <source>Do you want to delete recorded tracks from the list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>searchradio</name>
    <message>
        <location filename="../assets/searchradio.qml" line="24"/>
        <source>What radio station you are looking for?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/searchradio.qml" line="34"/>
        <source>Enter Radio Name ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/searchradio.qml" line="84"/>
        <source>No entries in this view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/searchradio.qml" line="115"/>
        <source>Loading data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/searchradio.qml" line="220"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/searchradio.qml" line="221"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>streamingradio</name>
    <message>
        <location filename="../assets/streamingradio.qml" line="40"/>
        <source>FM Radio</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>streamingradioFavorite</name>
    <message>
        <location filename="../assets/streamingradioFavorite.qml" line="45"/>
        <source>FM Radio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/streamingradioFavorite.qml" line="203"/>
        <source>Delete Favorite</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
