/*
 * Favorite.hpp
 *
 *  Created on: 8 avr. 2014
 *      Author: admin
 */

#ifndef FAVORITE_HPP_
#define FAVORITE_HPP_
#include <QObject>

class Favorite: public QObject
{
    Q_OBJECT

    // These are the properties that will be accessible by the datamodel in the view.
    Q_PROPERTY(QString favoriteID READ favoriteID WRITE setFavoriteID NOTIFY favoriteIDChanged FINAL)
    Q_PROPERTY(QString radioName READ radioName WRITE setRadioName NOTIFY radioNameChanged FINAL)
    Q_PROPERTY(QString frequenceName READ frequenceName WRITE setFrequenceName NOTIFY frequenceNameChanged FINAL)
    Q_PROPERTY(QString logo READ logo WRITE setLogo NOTIFY logoChanged FINAL)
    Q_PROPERTY(QString urlradio READ urlradio WRITE setUrlradio NOTIFY urlradioChanged FINAL)

public:
    Favorite(QObject *parent = 0);
    Favorite(const QString &id, const QString &radioName, const QString &frequenceName, const QString &logo, const QString &urlradio, QObject *parent = 0);

    QString favoriteID() const;
    QString radioName() const;
    QString frequenceName() const;
    QString logo() const;
    QString urlradio() const;


    void setFavoriteID(const QString &newId);
    void setRadioName(const QString &newRadio);
    void setFrequenceName(const QString &newFrequence);
    void setLogo(const QString &newLogo);
    void setUrlradio(const QString &newUrlradio);

Q_SIGNALS:
    void favoriteIDChanged(const QString &newId);
    void radioNameChanged(const QString &newRadio);
    void frequenceNameChanged(const QString &newFrequence);
    void logoChanged(const QString &newLogo);
    void urlradioChanged(const QString &newUrlradio);
private:
    QString m_id;
    QString m_radioName;
    QString m_frequenceName;
    QString m_logo;
    QString m_urlradio;

};



#endif /* FAVORITE_HPP_ */
