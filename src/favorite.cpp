/*
 * Favorite.cpp
 *
 *  Created on: 8 avr. 2014
 *      Author: admin
 */
#include "favorite.hpp"

Favorite::Favorite(QObject *parent)
    : QObject(parent)
{
}

Favorite::Favorite(const QString &id, const QString &radioName, const QString &frequenceName, const QString &logo, const QString &urlradio, QObject *parent)
    : QObject(parent)
    , m_id(id)
    , m_radioName(radioName)
    , m_frequenceName(frequenceName)
	, m_logo(logo)
    , m_urlradio(urlradio)
{
}

QString Favorite::favoriteID() const
{
    return m_id;
}

QString Favorite::radioName() const
{
    return m_radioName;
}

QString Favorite::frequenceName() const
{
    return m_frequenceName;
}
QString Favorite::logo() const
{
    return m_logo;
}
QString Favorite::urlradio() const
{
    return m_urlradio;
}

void Favorite::setFavoriteID(const QString &newId)
{
    if (newId != m_id) {
        m_id = newId;
        emit favoriteIDChanged(newId);
    }
}

void Favorite::setRadioName(const QString &newRadio)
{
    if (newRadio != m_radioName) {
        m_radioName = newRadio;
        emit radioNameChanged(newRadio);
    }
}

void Favorite::setFrequenceName(const QString &newFrequence)
{
    if (newFrequence != m_frequenceName) {
    	m_frequenceName = newFrequence;
        emit frequenceNameChanged(newFrequence);
    }
}

void Favorite::setLogo(const QString &newLogo)
{
    if (newLogo != m_logo) {
    	m_logo = newLogo;
        emit logoChanged(newLogo);
    }
}

void Favorite::setUrlradio(const QString &newUrlradio)
{
    if (newUrlradio != m_urlradio) {
    	m_urlradio = newUrlradio;
        emit urlradioChanged(newUrlradio);
    }
}



