/*
 * app.cpp
 *
 *  Created on: 8 avr. 2014
 *      Author: admin
 */
#ifndef APP_HPP
#define APP_HPP

#include <bb/cascades/GroupDataModel>

using namespace bb::cascades;

/*
 * @brief Declaration of our application's class (as opposed to the BB Cascades
 *  application class that contains our application).
 */
class App: public QObject
{
    // Classes that inherit from QObject must have the Q_OBJECT macro so
    // the meta-object compiler (MOC) can add supporting code to the application.
    Q_OBJECT

    // A property that is used by the list view in QML
    Q_PROPERTY(bb::cascades::DataModel* dataModel READ dataModel CONSTANT)

public:
    App();

    // "Q_INVOKABLE" allows these functions to be called from main.qml
    Q_INVOKABLE bool createRecord(const QString &radioName, const QString &frequenceName, const QString &logo, const QString &urlradio);
    Q_INVOKABLE void readRecords();
  //  Q_INVOKABLE bool updateRecord(const QString &key, const QString &firstName, const QString &lastName);
    Q_INVOKABLE bool deleteRecord(const QString &key);

private:
    // Functions to call upon initialization to setup the model and database
    void initDataModel();
    bool initDatabase();

    // To alert the user if something has gone wrong
    void showToastButton(const QString &message);

    // The getter method for the property
    bb::cascades::GroupDataModel* dataModel() const;

    // The data shown by the list view.
    GroupDataModel* m_dataModel;
};

#endif
