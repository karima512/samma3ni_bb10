#include "applicationui.hpp"
#include "app.cpp"
#include "TrackManager.hpp"
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/cascades/SceneCover>
#include <bb/cascades/Container>
#include <bps/navigator.h>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include "ExternalIP.hpp"

using namespace bb::cascades;

ApplicationUI::ApplicationUI(bb::cascades::Application *app) :
        QObject(app)
{
    // prepare the localization
    m_pTranslator = new QTranslator(this);
    m_pLocaleHandler = new LocaleHandler(this);

    if(!QObject::connect(m_pLocaleHandler, SIGNAL(systemLanguageChanged()), this, SLOT(onSystemLanguageChanged()))) {
        // This is an abnormal situation! Something went wrong!
        // Add own code to recover here
        qWarning() << "Recovering from a failed connect()";
    }
    // initial load
    const QString uuid(QLatin1String("9a6896a4-e2db-4ae0-a29f-3caf36442f3e"));
    registrationHandler = new RegistrationHandler(uuid, this);
    onSystemLanguageChanged();

    qmlRegisterType<ExternalIP>("Network.ExternalIP", 1, 0, "ExternalIP");
    // Create scene document from main.qml asset, the parent is set
    // to ensure the document gets destroyed properly at shut down.
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);
    qml->setContextProperty("_app", new App());
    qml->setContextProperty("app", this);
    qml->setContextProperty("_bbm", registrationHandler);
    qml->setContextProperty("_trackManager", new TrackManager(this->parent()));


	// set up the application's cover
    				QmlDocument *qmlCover = QmlDocument::create("asset:///Cover.qml").parent(this);
            	 	Container *rootContainer = qmlCover->createRootObject<Container>();
            	 	SceneCover *cover = SceneCover::create().content(rootContainer);
            	 	Application::instance()->setCover(cover);
    // Create root object for the UI
    AbstractPane *root = qml->createRootObject<AbstractPane>();

    // Set created root object as the application scene
    app->setScene(root);
}

void ApplicationUI::onSystemLanguageChanged()
{
    QCoreApplication::instance()->removeTranslator(m_pTranslator);
    // Initiate, load and install the application translation files.
    QString locale_string = QLocale().name();
    QString file_name = QString("RadioGulf_%1").arg(locale_string);
    if (m_pTranslator->load(file_name, "app/native/qm")) {
        QCoreApplication::instance()->installTranslator(m_pTranslator);
    }
}
void ApplicationUI::launchBrowser(QString url)
{
	navigator_invoke(url.toStdString().c_str(), 0);
}
