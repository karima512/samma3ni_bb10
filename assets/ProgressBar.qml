import bb.cascades 1.0

//! [0]
Container {
    id: root

    property int duration: 1
    property int position: 1

    bottomPadding: 20

    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }

    ProgressIndicator {
        verticalAlignment: VerticalAlignment.Center
        preferredWidth: 600

        fromValue: 0
        toValue: root.duration
        value: root.position
    }

    Label {
        verticalAlignment: VerticalAlignment.Center

        property int minutes: Math.floor(root.position/1000/60)
        property int seconds: Math.floor(root.position/1000%60)

        text: qsTr("%1:%2").arg(minutes < 10 ? "0" + minutes : "" + minutes)
                            .arg(seconds < 10 ? "0" + seconds : "" + seconds)
        textStyle {
            base: SystemDefaults.TextStyles.SmallText
            color: Color.Gray
        }
    }
}
//! [0]
