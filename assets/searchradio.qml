import bb.cascades 1.0
import QtQuick 1.0
import "js/parsersearch.js" as JS3
import Network.ExternalIP 1.0
import bb.system 1.0

Container {
    id: contentSearch
    property Page pageSearch
    property alias image: imagev.imageSource

    Container {

        layout: StackLayout {
            orientation: LayoutOrientation.TopToBottom
        }
        leftPadding: 20.0
        rightPadding: 20.0
        topPadding: 20.0
        bottomPadding: 20.0

        Label {
            id: label
            text: qsTr("What radio station you are looking for?")
            textStyle.fontWeight: FontWeight.Bold
        }
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }

            TextField {
                id: searchradio
                hintText: qsTr("Enter Radio Name ...")
            }

            Container {
                ImageView {
                    id: imagev
                    imageSource: "asset:///icones/search.png"
                    preferredHeight: 90
                    preferredWidth: 90
                }
                gestureHandlers: [
                    TapHandler {
                        onTapped: {
                            message.visible = true
                            loading.visible= false
                            myIndicator.visible = false
                            myIndicator.running = false
                            if (searchradio.text == null || searchradio.text == "")
                              {  
                                  loading.visible= false
                                  myIndicator.visible = false
                                  myIndicator.running = false
                                  dataModelSearch.clear();
                              }
                            else
                               { if (dataModelSearch.isEmpty()) {
                                myIndicator.running = true
                                myIndicator.visible = true
                                loading.visible = true
                                message.visible = false
                                } else {
                                myIndicator.running = false
                                myIndicator.visible = false
                                loading.visible = false
                                }
                                JS3.load(searchradio.text);
                                }
                            netip.getIP()
                        }
                    }
                ]
            }
        }
    }
    Container {
        topPadding: 10
        Label {
            horizontalAlignment: horizontalAlignment.Center
            verticalAlignment: verticalAlignment.Center
            id: message
            text: qsTr("No entries in this view")
            textStyle.fontWeight: FontWeight.Bold
            translationX: 160.0
            translationY: 330.0
            textStyle.fontSize: FontSize.Large
            textStyle.color: Color.Gray
            visible: true

        }
        Container {
            layout: AbsoluteLayout {

            }

            ActivityIndicator {
                id: myIndicator
                layoutProperties: AbsoluteLayoutProperties {
                    positionX: 260
                    positionY: 210
                }
                preferredHeight: 250
                preferredWidth: 250
                running: false
                visible: false
            }
            Label {
                id: loading
                layoutProperties: AbsoluteLayoutProperties {
                    positionX: 285
                    positionY: 450
                }
                text: qsTr("Loading data")
                textStyle.color: Color.Black
                visible: false
            }
        }
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.TopToBottom
            }
            ListView {
                id: myList
                onTriggered: {
                    selectedItemSearchRadio = dataModel.data(indexPath);
                    pageSearch = nextSearchRadio.createObject();
                    pageSearch.radioName = selectedItemSearchRadio.radio;
                    radioLabel = selectedItemSearchRadio.radioName;
                    pageSearch.frequenceName = selectedItemSearchRadio.frequence;
                    frequenceLabel = selectedItemSearchRadio.frequenceName;
                    pageSearch.logo = selectedItemSearchRadio.logo;
                    pageSearch.urlradio = selectedItemSearchRadio.urlradio;
                    navPane.push(pageSearch);
                    console.log("push ready")

                }
                listItemComponents: [
                    ListItemComponent {
                        Container {
                            ImageView {
                                horizontalAlignment: horizontalAlignment.Fill
                                verticalAlignment: verticalAlignment.Fill
                                imageSource: "asset:///icones/backgroundCell.png"
                            }
                            topMargin: 10
                            preferredWidth: 768
                            preferredHeight: 170
                            
                            layout: DockLayout {
                            }
                            Container {
                                leftPadding: 30
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                WebView {
                                    url: ListItemData.logo
                                    layoutProperties: StackLayoutProperties {
                                    }
                                    horizontalAlignment: HorizontalAlignment.Center
                                    maxHeight: 150
                                    maxWidth: 150
                                    minHeight: 150
                                    minWidth: 150
                                }

                                Container {
                                    leftMargin: 30
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.TopToBottom
                                    }
                                    Label {
                                        text: ListItemData.radio
                                        textStyle {
                                            base: SystemDefaults.TextStyles.TitleText
                                        }
                                    }

                                    Label {
                                        text: ListItemData.frequence + " " + "FM"
                                        textStyle {
                                            base: SystemDefaults.TextStyles.SubtitleText
                                            color: Color.create(0.4, 0.4, 0.4)
                                        }
                                    }
                                }
                            }
                            Divider {
                                verticalAlignment: VerticalAlignment.Bottom
                            }
                        }
                    }
                ]
                dataModel: ArrayDataModel {
                    id: dataModelSearch
                    onItemAdded: {
                        if (dataModelSearch.isEmpty()) {
                            message.visible = true;
                            myIndicator.running = false
                            myIndicator.visible = false
                            loading.visible = false

                        } else
                           { 
                            message.visible = false
	                        myIndicator.running = false
	                        myIndicator.visible = false
	                        loading.visible = false
	                        }
                    }
                }
            }
        }
    }
    attachedObjects: [
        SystemToast {
            id: toastinter
            body: qsTr("")
            button.label: qsTr("Ok")
            button.enabled: true
        },
        ExternalIP {
            id: netip
            onComplete: {
                if (info == "Http Error: 0") {
                    console.log("No internet")
                    myIndicator.running = false
                    myIndicator.visible = false
                    loading.visible = false
                    toastinter.body = "Check your Internet connection and try again."
                    toastinter.exec()
                   

                    //customdialog.open()
                } else {

                    console.log("Yes internet")

                }

            }
        }
    ]
}