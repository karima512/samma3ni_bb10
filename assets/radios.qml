import bb.cascades 1.0
import QtQuick 1.0
import "js/parserradios.js" as JS2
import Network.ExternalIP 1.0
import bb.system 1.0
import bb.multimedia 1.0


Page {
    property variant inter: {
    }

    property variant countryName: {
    }
    property variant countryId: {
    }

    property string selectedItemCategoryRadio

    property Page pageradio
Container {
    

    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.TopToBottom
        }
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            layout: DockLayout {}
            
            ImageView {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill                        
                imageSource: "asset:///images/titlebarfin.png"
            }
            
            Label {
                horizontalAlignment: HorizontalAlignment.Center
                
                text: qsTr ("All stations")
                textStyle.base: SystemDefaults.TextStyles.BigText
                textStyle.color: Color.White
            }
        }
        Container {
            id: loadingContainer
            layout: AbsoluteLayout {
            
            }
            horizontalAlignment: horizontalAlignment.Center
            verticalAlignment:VerticalAlignment.Center
            ActivityIndicator {
                id: myIndicator
                layoutProperties: AbsoluteLayoutProperties {
                    positionX: 260
                    positionY: 340
                }
                preferredHeight: 250
                preferredWidth: 250
                running: true
            }
            Label {
                id: loading
                layoutProperties: AbsoluteLayoutProperties {
                    positionX: 285
                    positionY: 580
                }
                text: qsTr("Loading data")
                textStyle.color: Color.Black
            }
        }
        Container {
            id: contentRadio
            topMargin: 10
            layout: StackLayout {
                orientation: LayoutOrientation.TopToBottom
            }
            ListView {
                id: myList
                onTriggered: {
                    selectedItemRadio = dataModel.data(indexPath);
                    pageradio = nextRadio.createObject();
                    pageradio.radioName = selectedItemRadio.radio;
                    radioLabel = selectedItemRadio.radio;
                    pageradio.frequenceName = selectedItemRadio.frequence;
                    frequenceLabel = selectedItemRadio.frequence;
                    pageradio.logo = selectedItemRadio.logo;
                    pageradio.urlradio = selectedItemRadio.urlradio;
                    navPane.push(pageradio);
                }
                listItemComponents: [
                    ListItemComponent {
                        Container {
                            ImageView {
                                horizontalAlignment: horizontalAlignment.Fill
                                verticalAlignment: verticalAlignment.Fill
                                imageSource: "asset:///icones/backgroundCell.png"
                            }
                            topMargin: 10
                            preferredWidth: 768
                            preferredHeight: 170

                            layout: DockLayout {
                            }

                            Container {
                                leftPadding: 30
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }

                                WebView {
                                    url: ListItemData.logo
                                    layoutProperties: StackLayoutProperties {
                                    }
                                    maxHeight: 150
                                    maxWidth: 150
                                    minHeight: 150
                                    minWidth: 150
                                }
                                Container {
                                    leftMargin: 30
                                    layout: StackLayout {
                                        orientation: LayoutOrientation.TopToBottom
                                    }
                                    Label {
                                        text: ListItemData.radio
                                        textStyle {
                                            base: SystemDefaults.TextStyles.TitleText
                                        }
                                    }

                                    Label {
                                        text: ListItemData.frequence + " " + "FM"
                                        textStyle {
                                            base: SystemDefaults.TextStyles.SubtitleText
                                            color: Color.create(0.4, 0.4, 0.4)
                                        }
                                    }
                                }
                            }
                            Divider {
                                verticalAlignment: VerticalAlignment.Bottom
                            }
                        }
                    }
                ]
                dataModel: ArrayDataModel {
                    id: dataModel
                    onItemAdded: {
                        if (dataModel.isEmpty()) {
                         myIndicator.running()
                        } else
                        {
                        myIndicator.stop()     
                        myIndicator.visible = false                     
                        loading.visible = false
                    }
                    }
                }

                onCreationCompleted: {
                    netip.getIP()
                    console.log("selectedItemCategory2", selectedItemCategory)
                    JS2.load(selectedItemCategory)

                }
            }
        }
       
        Container {
            background: actionbarpaint.imagePaint

            horizontalAlignment: HorizontalAlignment.Fill
            // verticalAlignment: VerticalAlignment.Bottom
            attachedObjects: [
                ImagePaintDefinition {
                    id: actionbarpaint
                    imageSource: "asset:///images/nav_action_bar.png"
                }
            ]
            layout: DockLayout {

            }
            Container {
                minHeight: 125
                minWidth: 150
                gestureHandlers: TapHandler {
                    onTapped: {
                        navPane.pop(); 
                    }
                }

            }

        }
    }
}
    attachedObjects: [
        
        SystemToast {
            id: toastinter
            body: ""
            button.label: qsTr("Ok")
            button.enabled: true
        },
        ExternalIP {
            id: netip
            onComplete: {
                if (info == "Http Error: 0") {
                    console.log("No internet")

                    toastinter.body = "Check your Internet connection and try again."
                    toastinter.exec()

                    //customdialog.open()
                } else {

                    console.log("Yes internet")

                }

            }
        }

        
    ]
}