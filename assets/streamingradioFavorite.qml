import bb.cascades 1.0
import bb.multimedia 1.0
import bb.system 1.0
import QtQuick 1.0
import bb.data 1.0
import "js/parserradios.js" as JS2

Page {
    id: root

    property bool databaseOpen: false
    property string pag
    property variant radioName: {
    }
    property variant frequenceName: {
    }
    property variant logo: {
    }
    property variant urlradio: {
    }

    Container {
        layout: DockLayout {

        }

        Container {
            
            layout: StackLayout {
                orientation: LayoutOrientation.TopToBottom
            }
            Container {
                horizontalAlignment: HorizontalAlignment.Center
                layout: DockLayout {}
                
                ImageView {
                    horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Fill                        
                    imageSource: "asset:///images/titlebarfin.png"
                }
                
                Label {
                    horizontalAlignment: HorizontalAlignment.Center
                    
                    text: qsTr ("FM Radio")
                    textStyle.base: SystemDefaults.TextStyles.BigText
                    textStyle.color: Color.White
                }
            }
            Container {
                topMargin: 100
                leftPadding: 200
                horizontalAlignment: horizontalAlignment.Center
                layout: StackLayout {
                    orientation: LayoutOrientation.TopToBottom
                }
                ImageView {
                    imageSource: "asset:///images/radio.png"
                    horizontalAlignment: HorizontalAlignment.Center
                    preferredHeight: 446
                    preferredWidth: 400                          
                }
            
            }
            Container {
                topPadding: 80
                // leftPadding: 80                
                horizontalAlignment: HorizontalAlignment.Center
                Label {
                    text: radioName
                    horizontalAlignment: HorizontalAlignment.Center
                    multiline: true
                    textStyle {
                        base: SystemDefaults.TextStyles.TitleText
                        color: Color.create("#da6973")
                        fontSize: FontSize.XLarge
                    }
                
                }
                Label {
                    text: frequenceName + " " + "FM"
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle {
                        base: SystemDefaults.TextStyles.SubtitleText
                        color: Color.create("#efbfc3")
                        fontSize: FontSize.Large
                    }
                }
            }  
            Container {
                leftPadding: 200
                topMargin: 80
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                
                ForeignWindowControl {
                    id: fwcVideoSurface
                    windowId: "myVideoSurface"
                    visible: boundToWindow
                    
                    updatedProperties: WindowProperty.Size |
                    WindowProperty.Position |
                    WindowProperty.Visible
                }
                
                ImageButton {
                    id: btnPlay
                    preferredWidth: 120
                    preferredHeight: 120
                    defaultImageSource: player.mediaState == MediaState.Started ? "asset:///images/playselected.png" :
                    "asset:///images/play.png"                  
                    onClicked: {
                        player.sourceUrl  = urlradio
                        player.play();
                    
                    }
                
                }
                ImageButton {
                    id: btnPause
                    preferredWidth: 120
                    preferredHeight: 120
                    defaultImageSource: player.mediaState == MediaState.Paused ? "asset:///images/pauseselected.png" :
                    "asset:///images/pause.png"  
                    onClicked: {                           
                        player.pause();
                    }
                }
                ImageButton {
                    id: btnStop
                    preferredWidth: 120
                    preferredHeight: 120
                    defaultImageSource: "asset:///images/stop.png"
                    pressedImageSource: "asset:///images/stopselected.png"
                    onClicked: {
                        player.stop();
                        player.resset()
                    
                    }
                }
            }
        }
        Container {
            background: actionbarpaint.imagePaint
            // topMargin: 450
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Bottom
            attachedObjects: [
                ImagePaintDefinition {
                    id: actionbarpaint
                    imageSource: "asset:///images/nav_action_bar.png"
                }
            ]
            layout: DockLayout {

            }
            Container {
                minHeight: 125
                minWidth: 150
                gestureHandlers: TapHandler {
                    onTapped: {
                        navPane.pop();

                    }
                }

            }

            Container {
                leftPadding: 100
                layout: DockLayout {
                }
                minWidth: 550
                horizontalAlignment: HorizontalAlignment.Right
                verticalAlignment: VerticalAlignment.Center

                ImageButton {
                    defaultImageSource: "asset:///images/record.png"
                    horizontalAlignment: HorizontalAlignment.Left
                    onClicked: {
                        navPane.push(recordPage.createObject())
                    }
                }

                ImageButton {
                    id: favoriteButton
                    defaultImageSource: "asset:///images/delete.png"
                    horizontalAlignment: HorizontalAlignment.Center
                    onClicked: {
                        deleteFavorite.exec()
                        messageDelete = true 
                    }

                }

            }
        }
    }
    attachedObjects: [
        SystemDialog {
            id: deleteFavorite
            title: qsTr("Delete Favorite")
            body: qsTr("Do you want to delete " + nameRadioToast + " from the list")
            onFinished: {
                if (deleteFavorite.result == SystemUiResult.ConfirmButtonSelection) {
                    console.log("Selected: " + selectedID)
                     _app.deleteRecord(selectedID)                    
                    _app.readRecords()
                    navPane.pop()
                    messageDelete = true 
                }
            }

        }
    ]
}