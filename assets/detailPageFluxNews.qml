import bb.cascades 1.0

Page {

    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.TopToBottom
        }
        Container {
            layout: DockLayout {
                
            }
       
        ScrollView {
            
            scrollViewProperties.scrollMode: ScrollMode.Vertical
            Container {
                
                
                WebView {
                    
                    url: urlNews
                    
                }
            
            }
        }
    }  
        Container {
            background: actionbarpaint.imagePaint
            
            horizontalAlignment: HorizontalAlignment.Fill
            // verticalAlignment: VerticalAlignment.Bottom
            attachedObjects: [
                ImagePaintDefinition {
                    id: actionbarpaint
                    imageSource: "asset:///images/nav_action_bar.png"
                }
            ]
            layout: DockLayout {
            
            }
            Container {
                minHeight: 125
                minWidth: 150
                gestureHandlers: TapHandler {
                    onTapped: {
                        navPane.pop();
                    
                    }
                }
            
            }
        
        }
    }
}
