import bb.cascades 1.0
import bb.multimedia 1.0

Container {
    id: containerRecording
    
    ControlDelegate {
        id: selectedContDelegate
    }
    onCreationCompleted: {
        selectedContDelegate.source = "record.qml"
    }

}