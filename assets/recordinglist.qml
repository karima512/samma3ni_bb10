import bb.cascades 1.0
import bb.multimedia 1.0
import bb.system 1.0
Container {


    layout: StackLayout {
        orientation: LayoutOrientation.TopToBottom
    }
    Container {
        horizontalAlignment: HorizontalAlignment.Center
        layout: DockLayout {
        }

        ImageView {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            imageSource: "asset:///images/titlebarfin.png"
        }

        Label {
            horizontalAlignment: HorizontalAlignment.Center

            text: qsTr("Recorded Tracks")
            textStyle.base: SystemDefaults.TextStyles.BigText
            textStyle.color: Color.White
        }
    }
        

    Container {
        
        layout: DockLayout {}
     
        Label {
            horizontalAlignment: horizontalAlignment.Center
            verticalAlignment: verticalAlignment.Center
            id: message
            text: qsTr("No entries in this view")
            textStyle.fontWeight: FontWeight.Bold
            translationX: 160.0
            translationY: 470.0
            textStyle.fontSize: FontSize.Large
            textStyle.color: Color.Gray
            visible: true
        
        }
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            leftPadding: 30
            topPadding: 30
            rightPadding: 30
            bottomPadding: 30
   
            // The recorded tracks list view
            ListView {
                id: listView
                
                horizontalAlignment: HorizontalAlignment.Center
                topMargin: 50
                
                dataModel: _trackManager.model
                
                listItemComponents: ListItemComponent {
                    type: "item"
                    StandardListItem {
                        title: ListItemData.name
                    }
                }
                contextActions: [
                    ActionSet {
                        title: qsTr("Delete")
                        subtitle: qsTr("Delete all recorded tracks")
                        
                        actions: [
                            DeleteActionItem {
                                title: qsTr("Clear All Tracks")
                                onTriggered: {
                                    deleteRecord.exec()
                                    messageDelete = true
                                    if (_trackManager.model.isEmpty())
                                    {
                                        message.visible = true  
                                    }
                                    else 
                                        message.visible = false
                                }
                            }
                        ]
                    } // end of ActionSet   
                ] // end of contextActions list
                onTriggered: {
                    clearSelection()
                    select(indexPath)
                    playerUrlTrack =  "file://" + listView.dataModel.data(listView.selected()).url
                    tracking = (listView.dataModel.data(listView.selected()).name).replace(".m4a","") 
                    console.log("nameTrack", tracking) 
                    navPane.push(playRecordedTrackRadio.createObject()) 
                }
            }

        }
    }

		onCreationCompleted: {
            if (_trackManager.model.isEmpty())
			      {
			        message.visible = true  
			      }
            else 
            		message.visible = false
        }
        attachedObjects: [
            SystemDialog {
                id: deleteRecord
                title: qsTr("Delete recorded tracks ")
                body: qsTr("Do you want to delete recorded tracks from the list")
                onFinished: {
                    if (deleteRecord.result == SystemUiResult.ConfirmButtonSelection) {
                        _trackManager.clearAllTracks()                                         
                    }
                }
            
            }
        ]
}