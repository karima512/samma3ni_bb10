import bb.cascades 1.0
import bb.multimedia 1.0


Page {
    
    id: mainPage
    DictaphoneScrollView {

            Container {
                layout: DockLayout {
                }
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }

                Container {
                    horizontalAlignment: HorizontalAlignment.Center
                    layout: DockLayout {}
                    
                    ImageView {
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill                        
                        imageSource: "asset:///images/titlebarfin.png"
                    }
                    
                    Label {
                        horizontalAlignment: HorizontalAlignment.Center
                        
                        text: qsTr ("Recording")
                        textStyle.base: SystemDefaults.TextStyles.BigText
                        textStyle.color: Color.White
                    }
                }
                Container {
                    topMargin: 50
                    leftPadding: 70
                    horizontalAlignment: horizontalAlignment.Center
                    layout: DockLayout {
                       
                    }
                    ImageView {
                        imageSource: "asset:///images/recording.png"
                        horizontalAlignment: HorizontalAlignment.Center
                        preferredHeight: 650
                        preferredWidth: 650                          
                    }
//                    Led {
//                        id: led
//                        horizontalAlignment: HorizontalAlignment.Left
//                        verticalAlignment: VerticalAlignment.Top
//                        translationX: 400
//                        translationY: 500
//                        state: (recorder.mediaState == MediaState.Started ? "rec" : recorder.mediaState == MediaState.Paused ? "pause" : "off")
//                    }
                
                }
                Container {
                    topPadding: 20
                    horizontalAlignment: HorizontalAlignment.Center
                    Label {
                        horizontalAlignment: HorizontalAlignment.Center
                        
                        text: radioLabel
                        textStyle {
                            base: SystemDefaults.TextStyles.TitleText
                            color: Color.create("#da6973")
                            fontSize: FontSize.XLarge
                        }
                    }
                    Label {
                        id : frequence
                        horizontalAlignment: HorizontalAlignment.Center
                        
                        text: frequenceLabel + " FM"
                        textStyle {
                            base: SystemDefaults.TextStyles.SubtitleText
                            color: Color.create("#efbfc3")
                            fontSize: FontSize.Large
                        }
                        
                    }
                
                }
              
                Container {
                    leftPadding: 200
                    topPadding: 40
                    bottomPadding: 150
                    id: buttonContainer
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Bottom
                    
                    preferredWidth: 768
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    
                    //! [2]
                    // The 'Record' button
                    ImageToggleButton {
                        rightMargin: 2
                        preferredWidth: 120
                        preferredHeight: 120
                        imageSourceDefault: "asset:///images/play.png"
                        imageSourcePressedUnchecked: "asset:///images/play.png"
                        imageSourceChecked: "asset:///images/playselected.png"
                        imageSourcePressedChecked: "asset:///images/playselected.png"
                        imageSourceDisabledChecked: "asset:///images/playselected.png"
                        enabled: (recorder.mediaState != MediaState.Paused)
                        onCheckedChanged: {
                            if (recorder.mediaState == MediaState.Started) {
                                // Stop the recorder
                                recorder.reset()
                                
                                // Update the internal track list
                                _trackManager.update()
                                
                                // Play the finished sound
                                recordStopSound.play()
                            } else {
                                // Update the internal track list
                                _trackManager.update()
                                
                                // Configure the recorder to use a new URL
                                recorder.outputUrl = "file://" + _trackManager.nextTrackUrl()
                                
                                // Play the start sound
                                recordStartSound.play()
                                
                                // Start the recorder
                                recorder.record()
                            }
                        }
                    }
                    //! [2]
                    
                    //! [3]
                    // The 'Pause' button
                    ImageToggleButton {
                        leftMargin: 2
                        rightMargin: 2
                        preferredWidth: 120
                        preferredHeight: 120
                        imageSourceDefault: "asset:///images/pause.png"
                        imageSourceDisabledUnchecked: "asset:///images/pause.png"
                        imageSourcePressedUnchecked: "asset:///images/pauseselected.png"
                        imageSourceChecked: "asset:///images/pauseselected.png"
                        imageSourcePressedChecked: "asset:///images/pauseselected.png"
                        enabled: (recorder.mediaState == MediaState.Started || recorder.mediaState == MediaState.Paused)
                        onCheckedChanged: {
                            if (recorder.mediaState == MediaState.Started) recorder.pause(); else recorder.record()
                        }
                    }
                    //! [3]
                    
                    //! [4]
                    // The 'Play' button
                    ImageButton {
                        leftMargin: 2
                        preferredWidth: 120
                        preferredHeight: 120
                        defaultImageSource: "asset:///images/list.png"
                        pressedImageSource: "asset:///images/listselected.png"
                        disabledImageSource: "asset:///images/list.png"
                        enabled: (_trackManager.hasRecordedTracks && recorder.mediaState != MediaState.Started && recorder.mediaState != MediaState.Paused)
                        onClicked: navPane.push(playerPage.createObject())
                    }
                    //! [4]
                }
            }
            Container {
                background: actionbarpaint.imagePaint
                // topMargin: 100
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Bottom
                attachedObjects: [
                    ImagePaintDefinition {
                        id: actionbarpaint
                        imageSource: "asset:///images/nav_action_bar.png"
                    }
                ]
                layout: DockLayout {
                
                }
                Container {
                    minHeight: 125
                    minWidth: 150
                    gestureHandlers: TapHandler {
                        onTapped: {
                            navPane.pop();
                        
                        }
                    }
                
                }
            
            }
        }
    }

    //! [5]
    attachedObjects: [
        AudioRecorder {
            id: recorder
        },
        ComponentDefinition {
            id: playerPage
            source: "PlayerPage.qml"
        },
        SystemSound {
            id: recordStartSound
            sound: SystemSound.RecordingStartEvent
        },
        SystemSound {
            id: recordStopSound
            sound: SystemSound.RecordingStopEvent
        }
//        ,
//        // application supports changing the Orientation
//        OrientationHandler {
//            // onOrientationChanged: { should be this from docs, but onOrientationAboutToChange runs smoother
//            onOrientationAboutToChange: {
//                mainPage.reLayout(orientation);
//            }
//        }
    ]
    //! [5]

    function reLayout(orientation) {
        if (orientation == UIOrientation.Landscape) {
            imageBackground.imageSource = "asset:///images/backgroundRadio.png"
            led.translationY = 40
            tape.translationY = 120
            tape.translationX = -30
            tape.horizontalAlignment = HorizontalAlignment.Right
            label.verticalAlignment = VerticalAlignment.Top
            label.translationY = 50
            label.translationX = 260
            buttonBackground.horizontalAlignment = HorizontalAlignment.Right
            buttonBackground.translationX = -40
            buttonContainer.horizontalAlignment = HorizontalAlignment.Right
            buttonContainer.translationX = -40
        } else {
            imageBackground.imageSource = "asset:///images/backgroundRadio.png"
            led.translationY = 500
            tape.translationY = 570
            tape.translationX = 0
            tape.horizontalAlignment = HorizontalAlignment.Center
            label.verticalAlignment = VerticalAlignment.Bottom
            label.translationY = -205
            label.translationX = 0
            buttonBackground.horizontalAlignment = HorizontalAlignment.Center
            buttonBackground.translationX = 0
            buttonContainer.horizontalAlignment = HorizontalAlignment.Center
            buttonContainer.translationX = 0
        }
    }

//    onCreationCompleted: {
//        OrientationSupport.supportedDisplayOrientation = SupportedDisplayOrientation.All;
//        reLayout(OrientationSupport.orientation);
//    }
}
