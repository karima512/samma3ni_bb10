import bb.cascades 1.0
//! [0]
Container {
    layout: DockLayout {
    }

    background: Color.Red

    Label {
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center

        text: qsTr("ERROR: Database Initialization Failed")
    }
}
//! [0]