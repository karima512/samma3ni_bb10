import bb.cascades 1.0
import bb.multimedia 1.0

Page {
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.TopToBottom        
        }
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            layout: DockLayout {
            }
            
            ImageView {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                imageSource: "asset:///images/titlebarfin.png"
            }
            
            Label {
                horizontalAlignment: HorizontalAlignment.Center
                
                text: qsTr("Play Recorded Tracks")
                textStyle.base: SystemDefaults.TextStyles.BigText
                textStyle.color: Color.White
            }
           
        }
     
   //     verticalAlignment: verticalAlignment.Bottom
   Container {
       layout: DockLayout {
           
       }
       ImageView {
           horizontalAlignment: horizontalAlignment.Fill
           verticalAlignment: verticalAlignment.Fill
           imageSource: "asset:///images/backgroundplayer.png"
       }
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.TopToBottom
            }
            // leftPadding: 0
            topPadding: 615
            bottomPadding: 100
            verticalAlignment: verticalAlignment.Bottom
            Container {
                bottomPadding: 200
                leftPadding: 280
                horizontalAlignment: horizontalAlignment.Center
                verticalAlignment: verticalAlignment.Center
                layout: StackLayout {
                    
                }
                Label {
                    text: tracking 
                    horizontalAlignment: HorizontalAlignment.Center
                    textStyle {
                        base: SystemDefaults.TextStyles.TitleText
                        color: Color.create("#da6973")
                        fontSize: FontSize.XLarge
                    }
                }
            }
            Container{
            layout: StackLayout {
                
                orientation: LayoutOrientation.LeftToRight
            
            }
            horizontalAlignment: horizontalAlignment.Fill
          //  leftPadding: 10
          //  rightPadding: 10
            //! [0]
            ImageButton {
                
                preferredWidth: 63
                preferredHeight: 63
                
                defaultImageSource: player.mediaState == MediaState.Started ? "asset:///images/stop.png" :
                player.mediaState == MediaState.Paused  ? "asset:///images/pause.png" :
                "asset:///images/play.png"  
                
                onClicked: {
                    if (player.mediaState == MediaState.Started)
                        nowPlaying.revoke()
                    else 
                        
                        nowPlaying.acquire()
                
                }
            
            }
            //! [0]
            
            //! [1]
            ProgressBar {
                topPadding: 20
              // preferredWidth: 660
               // horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                
                duration: nowPlaying.duration
                position: nowPlaying.position
            }
            //! [1]
        }
        }
}      
        attachedObjects: [
            //! [2]
            MediaPlayer {
                id: player
                
                sourceUrl: playerUrlTrack
            },
            //! [2]
            
            //! [3]
            NowPlayingConnection {
                id: nowPlaying
                
                duration: player.duration
                position: player.position
                // iconUrl: "asset:///images/music.png"
                mediaState: player.mediaState
                
                onAcquired: {
                    player.play()
                    
                    nowPlaying.mediaState = MediaState.Started
                    nowPlaying.metaData = player.metaData
                }
                
                onPause: {
                    player.pause()
                
                }
                
                onPlay: {
                    player.play()
                }
                
                onRevoked: {
                    player.stop()
                
                }
            }
        //! [3]
        ]
        
        Container {
            background: actionbarpaint.imagePaint
            // topMargin: 450
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Bottom
            attachedObjects: [
                ImagePaintDefinition {
                    id: actionbarpaint
                    imageSource: "asset:///images/nav_action_bar.png"
                }
            ]
            layout: DockLayout {
            
            }
            Container {
                minHeight: 125
                minWidth: 150
                gestureHandlers: TapHandler {
                    onTapped: {
                        navPane.pop()
                        // navPane.navigateTo(navPane.nextPage)
                    }
                }
            
            }
        }
    }
    
}
