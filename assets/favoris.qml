import bb.cascades 1.0
import bb.system 1.0

Container {
    
    
    property Page pagefavorite

    layout: DockLayout {

    }
    
//    contextActions: [
//        ActionSet {
//            title: "Delete Favorite"
//            //    subtitle: "This is an action set."
//
//            actions: [
//                DeleteActionItem {
//                    onTriggered: {
//                        deleteFavorite.exec()
//                    }         
//                }
//            ]
//        } // end of ActionSet
//    ] // end of contextActions list
    Container {
        horizontalAlignment: HorizontalAlignment.Center
        layout: DockLayout {}
        
        ImageView {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill                        
            imageSource: "asset:///images/titlebarfin.png"
        }
        
        Label {
            horizontalAlignment: HorizontalAlignment.Center
            
            text: qsTr ("Favorites")
            textStyle.base: SystemDefaults.TextStyles.BigText
            textStyle.color: Color.White
        }
    }
    Label {
        horizontalAlignment: horizontalAlignment.Center
        verticalAlignment: verticalAlignment.Center
        id: message
        text: qsTr("No entries in this view")
        textStyle.fontWeight: FontWeight.Bold
        translationX: 160.0
        translationY: 580.0
        textStyle.fontSize: FontSize.Large
        textStyle.color: Color.Gray
        visible: false
    
    }
    
    ListView {
        topPadding: 120
        dataModel: _app.dataModel
        
        id: myList
        onTriggered: {
            selectedItemFavorite = dataModel.data(indexPath);
            selectedID = selectedItemFavorite.favoriteID
            console.log("Selecteditem: " + selectedID)
            pagefavorite = deleteFavoritePage.createObject();
            pagefavorite.radioName = selectedItemFavorite.radioName;
            radioLabel = selectedItemFavorite.radioName;
            nameRadioToast = selectedItemFavorite.radioName
            pagefavorite.frequenceName = selectedItemFavorite.frequenceName;
            frequenceLabel = selectedItemFavorite.frequenceName;
            pagefavorite.logo = selectedItemFavorite.logo;
            pagefavorite.urlradio = selectedItemFavorite.urlradio;
            navPane.push(pagefavorite);

        }

        listItemComponents: [
            ListItemComponent {
                type: "item"
                Container {
                    ImageView {
                        horizontalAlignment: horizontalAlignment.Fill
                        verticalAlignment: verticalAlignment.Fill
                        imageSource: "asset:///icones/backgroundCell.png"
                    }
                    topMargin: 10
                    preferredWidth: 768
                    preferredHeight: 170

                    layout: DockLayout {
                    }
                    Container {
                        leftPadding: 30
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        WebView {
                            url: ListItemData.logo
                            layoutProperties: StackLayoutProperties {
                            }
                            horizontalAlignment: HorizontalAlignment.Center
                            maxHeight: 150
                            maxWidth: 150
                            minHeight: 150
                            minWidth: 150
                        }

                        Container {
                            leftMargin: 30
                            layout: StackLayout {
                                orientation: LayoutOrientation.TopToBottom
                            }
                            Label {
                                text: ListItemData.radioName
                                textStyle {
                                    base: SystemDefaults.TextStyles.TitleText
                                }
                            }

                            Label {
                                text: ListItemData.frequenceName + " " + "FM"
                                textStyle {
                                    base: SystemDefaults.TextStyles.SubtitleText
                                    color: Color.create(0.4, 0.4, 0.4)
                                }
                            }
                        }
                    }
                    Divider {
                        verticalAlignment: VerticalAlignment.Bottom
                    }

                }

            }

        ]
    }
    onCreationCompleted: {
        _app.readRecords(); 
     //   deleteToast.exec()
        if (_app.dataModel.isEmpty()|| messageDelete == true ) 
        		{                
                    message.visible = true           
	            } else
	            { 
	                message.visible = false
	            }
    }

    attachedObjects: [

        SystemToast {
            id: deleteToast
            body: qsTr("Long press an item to remove from list after open it")
            button.label: qsTr("Ok")
            button.enabled: true
        },
        SystemToast {
            id: deleteFavorite
            body: qsTr("Do you want to delete " + nameRadioToast + "from the list")
            button.label: qsTr("Ok")
            button.enabled: true
            onFinished: {
                console.log("Selected: " + selectedID)
                _app.deleteRecord(selectedID)
                _app.readRecords()
           }
            
        }
    ]
}