import bb.cascades 1.0
import bb.multimedia 1.0

//! [0]
Page {
    Container {
        
        layout: StackLayout {
            orientation: LayoutOrientation.TopToBottom
        }
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            layout: DockLayout {
            }
            
            ImageView {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                imageSource: "asset:///images/titlebarfin.png"
            }
            
            Label {
                horizontalAlignment: HorizontalAlignment.Center
                
                text: qsTr("Recorded Tracks")
                textStyle.base: SystemDefaults.TextStyles.BigText
                textStyle.color: Color.White
            }
        }
        
        
        Container {
            
            layout: DockLayout {}
            
            Label {
                horizontalAlignment: horizontalAlignment.Center
                verticalAlignment: verticalAlignment.Center
                id: message
                text: "No entries in this view"
                textStyle.fontWeight: FontWeight.Bold
                translationX: 160.0
                translationY: 470.0
                textStyle.fontSize: FontSize.Large
                textStyle.color: Color.Gray
                visible: true
            
            }
            Container {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                
                leftPadding: 30
                topPadding: 30
                rightPadding: 30
                bottomPadding: 30
                
                // The recorded tracks list view
                ListView {
                    id: listView
                    
                    horizontalAlignment: HorizontalAlignment.Center
                    topMargin: 50
                    
                    dataModel: _trackManager.model
                    
                    listItemComponents: ListItemComponent {
                        type: "item"
                        StandardListItem {
                            title: ListItemData.name
                        }
                    }
                    contextActions: [
                        ActionSet {
                            title: "Delete"
                            subtitle: "Delete all recorded tracks"
                            
                            actions: [
                                DeleteActionItem {
                                    title: "Clear All Tracks"
                                    onTriggered: {
                                        _trackManager.clearAllTracks()
                                        if (_trackManager.model.isEmpty())
                                        {
                                            message.visible = true  
                                        }
                                        else 
                                            message.visible = false
                                    }
                                }
                            ]
                        } // end of ActionSet   
                    ] // end of contextActions list
                    onTriggered: {
                        clearSelection()
                        select(indexPath)
                        playerUrlTrack =  "file://" + listView.dataModel.data(listView.selected()).url
                        tracking = (listView.dataModel.data(listView.selected()).name).replace(".m4a","") 
                        console.log("nameTrack", tracking) 
                        navPane.push(playRecordedTrackRadio.createObject()) 
                    }
                }
                
               
            }
        }
        onCreationCompleted: {
            if (_trackManager.model.isEmpty())
            {
                message.visible = true  
            }
            else 
                message.visible = false
        }
        Container {
            background: actionbarpaint.imagePaint
            
            horizontalAlignment: HorizontalAlignment.Fill
            // verticalAlignment: VerticalAlignment.Bottom
            attachedObjects: [
                ImagePaintDefinition {
                    id: actionbarpaint
                    imageSource: "asset:///images/nav_action_bar.png"
                }
            ]
            layout: DockLayout {
            
            }
            Container {
                minHeight: 125
                minWidth: 150
                gestureHandlers: TapHandler {
                    onTapped: {
                        navPane.pop();
                    
                    }
                }
            
            }
        
        }
    }
}
//! [0]
