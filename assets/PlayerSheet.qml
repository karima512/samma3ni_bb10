import bb.cascades 1.0
import bb.multimedia 1.0

//! [0]
Sheet {
    id: playerSheet

    Page {
        Container {
            layout: DockLayout {}

            // The background image
            ImageView {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill

                imageSource: "asset:///images/nav_action_bar.png"
            }

            Container {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill

                leftPadding: 30
                topPadding: 30
                rightPadding: 30
                bottomPadding: 30

                // The title label
                Label {
                    horizontalAlignment: HorizontalAlignment.Center

                    text: qsTr ("Recorded Tracks")
                    textStyle {
                        base: SystemDefaults.TextStyles.BigText
                        color: Color.White
                    }
                }

                // The recorded tracks list view
                ListView {
                    id: listView

                    horizontalAlignment: HorizontalAlignment.Center
                    topMargin: 50

                    dataModel: _trackManager.model

                    listItemComponents: ListItemComponent {
                        type: "item"
                        StandardListItem {
                            title: ListItemData.name
                        }
                    }

                    onTriggered: {
                        clearSelection()
                        select(indexPath)
                    }
                }

                // The 'Play' button
                Button {
                    horizontalAlignment: HorizontalAlignment.Center
                    topMargin: 50

                    text: qsTr ("Play")

                    onClicked: {
                        // Reset URL of player
                        player.sourceUrl = ""

                        // Set the currently selected track as player source URL
                        player.sourceUrl = listView.dataModel.data(listView.selected()).url

                        // Start playback
                        player.play()
                    }
                }
            }
            Container {
                background: actionbarpaint.imagePaint
                // topMargin: 450
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Bottom
                attachedObjects: [
                    ImagePaintDefinition {
                        id: actionbarpaint
                        imageSource: "asset:///images/nav_action_bar.png"
                    }
                ]
                layout: DockLayout {
                
                }
                Container {
                    minHeight: 125
                    minWidth: 150
                    gestureHandlers: TapHandler {
                        onTapped: {
                            playerSheet.close()
                            // navPane.navigateTo(navPane.nextPage)
                        }
                    }
                
                }
                
                Container {
                    leftPadding: 100
                    layout: DockLayout {
                    }
                    minWidth: 550
                    horizontalAlignment: HorizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center
                    ImageButton {
                        defaultImageSource: "asset:///images/delete.png"
                        horizontalAlignment: HorizontalAlignment.Center
                        onClicked: {
                           _trackManager.clearAllTracks()
                           playerSheet.close()
                        }
                    
                    }
                
                }
            }
        }

//        actions: [
//            ActionItem {
//                title: qsTr ("Back")
//                ActionBar.placement: ActionBarPlacement.OnBar
//
//                onTriggered: {
//                    playerSheet.close()
//                }
//            },
//            ActionItem {
//                title: qsTr ("Clear All Tracks")
//                ActionBar.placement: ActionBarPlacement.OnBar
//
//                onTriggered: {
//                    _trackManager.clearAllTracks()
//                    playerSheet.close()
//                }
//            }
//        ]

//        attachedObjects: [
//            MediaPlayer {
//                id: player
//            }
//        ]
    }
}
//! [0]
