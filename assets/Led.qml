import bb.cascades 1.0

//! [0]
ImageView {
    property string state: "off"

    imageSource: (state == "rec" ? "asset:///images/led_rec.png" :
                  state == "pause" ? "asset:///images/led_pause.png" :
                  "asset:///images/led_off.png")
}
//! [0]
