import bb.cascades 1.0
import bb.data 1.0
import Network.ExternalIP 1.0
import bb.system 1.0

Container {
    layout: StackLayout {
        orientation: LayoutOrientation.TopToBottom
    }

    Container {
        Container {
            layout: DockLayout {
            }

            Container {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill

                Container {
                    horizontalAlignment: HorizontalAlignment.Center
                    layout: DockLayout {
                    }

                    ImageView {
                        horizontalAlignment: HorizontalAlignment.Fill
                        verticalAlignment: VerticalAlignment.Fill
                        imageSource: "asset:///images/titlebarfin.png"
                    }

                    Label {
                        horizontalAlignment: HorizontalAlignment.Center

                        text: qsTr("Radio News Feeds")
                        textStyle.base: SystemDefaults.TextStyles.BigText
                        textStyle.color: Color.White
                    }
                }

                //! [1]
                // A list view that shows all the categories of a news source
                ListView {
                    dataModel: feedsDataModel
                    listItemComponents: [
                        ListItemComponent {
                            type: "item"
                            Container {
                                leftPadding: 30
                                preferredWidth: 768
                                preferredHeight: 100

                                layout: DockLayout {
                                }

                                Label {
                                    verticalAlignment: VerticalAlignment.Center

                                    text: ListItemData.name

                                    textStyle.base: SystemDefaults.TextStyles.TitleText
                                    textStyle.color: Color.Black
                                }

                                Divider {
                                    verticalAlignment: VerticalAlignment.Bottom
                                }
                            }
                        }
                    ]

                    onTriggered: {
                        var feedItem = feedsDataModel.data(indexPath);
                        articlesDataSource.source = "http://" + feedItem.feed;
                        articlesDataSource.load();

                        var page = newsListings.createObject();
                        page.title = "News" + ": " + feedItem.name
                        navPane.push(page);
                    }
                    onCreationCompleted: {
                        feedsDataSource.source = "jsonFiles/radionews.json";
                        feedsDataSource.load();
                        netip.getIP()
                    }
                }
                //! [1]
            }
        }
    }

    attachedObjects: [
        //! [2]
        // The data model that contains the content of a JSON file
        GroupDataModel {
            id: feedsDataModel
            sortingKeys: [ "order" ]
            grouping: ItemGrouping.None
        },

        // The data source that fills the above model with the content of a JSON file
        DataSource {
            id: feedsDataSource
            source: ""
            onDataLoaded: {
                feedsDataModel.clear();
                feedsDataModel.insertList(data);
            }
            onError: {
                console.log("JSON Load Error: [" + errorType + "]: " + errorMessage);
            }
        },
        //! [2]

        //! [3]
        // The data model that contains the articles from a RSS feed
        GroupDataModel {
            id: articlesDataModel
            sortingKeys: [ "pubDate" ]
            sortedAscending: false
            grouping: ItemGrouping.None
            
//            onItemAdded: {
//                if (articlesDataModel.isEmpty()) {
//                    myIndicator.running()
//                } else
//                    myIndicator.stop()
//                loading.visible = false
//            }
        },

        // The data source that fills the above model with the articles
        DataSource {
            id: articlesDataSource
            source: ""
            query: "/rss/channel/item"
            type: DataSource.Xml
            onDataLoaded: {
                articlesDataModel.clear();
                articlesDataModel.insertList(data);
            }
            onError: {
                console.log("RSS Load Error[" + errorType + "]: " + errorMessage);
            }

        },
        //! [3]

        //! [4]
        // The dynamically loaded page to show the list of articles from a RSS feed
        ComponentDefinition {
            id: newsListings
            Page {
                property variant anim
                property alias title: titleLabel.text

                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.TopToBottom
                    }

                    //                    ImageView {
                    //                        horizontalAlignment: HorizontalAlignment.Fill
                    //                        verticalAlignment: VerticalAlignment.Fill
                    //
                    //                        imageSource: "asset:///images/background.png"
                    //                    }

                    Container {
                        layout: DockLayout {
                        }
//                        Container {
//                            layout: AbsoluteLayout {
//                            
//                            }
//                            
//                            ActivityIndicator {
//                                id: myIndicator
//                                layoutProperties: AbsoluteLayoutProperties {
//                                    positionX: 270
//                                    positionY: 340
//                                }
//                                preferredHeight: 250
//                                preferredWidth: 250
//                                running: true
//                            }
//                            Label {
//                                id: loading
//                                layoutProperties: AbsoluteLayoutProperties {
//                                    positionX: 295
//                                    positionY: 580
//                                }
//                                text: qsTr("Loading data")
//                                textStyle.color: Color.Black
//                            }
//                        }
                        Container {
                            horizontalAlignment: HorizontalAlignment.Center
                            layout: DockLayout {
                            }

                            ImageView {
                                horizontalAlignment: HorizontalAlignment.Fill
                                verticalAlignment: VerticalAlignment.Fill

                                imageSource: "asset:///images/titlebarfin.png"
                            }

                            Label {
                                id: titleLabel
                                horizontalAlignment: HorizontalAlignment.Center

                                textStyle.base: SystemDefaults.TextStyles.BigText
                                textStyle.color: Color.White
                            }
                        }

                        ListView {
                            topPadding: 120
                            dataModel: articlesDataModel
                            listItemComponents: [
                                ListItemComponent {
                                    type: "item"
                                    ArticleItem {
                                        title: ListItemData.title
                                        pubDate: ListItemData.pubDate
                                    }
                                }
                            ]
                            onTriggered: {
                                //    app.launchBrowser("http://www.nojoomplus.com/news/1056-shakira-avec-son-fils-milan-lors-de-la-populaire-emission-de-la-voix.html");

                                var feedItem = articlesDataModel.data(indexPath);

                                //pageNews = detailPage.createObject();
                                urlNews = feedItem.link;
                                navPane.push(detailPage.createObject());
                            }
                        }
                    }

                    Container {
                        background: actionbarpaint.imagePaint

                        horizontalAlignment: HorizontalAlignment.Fill
                        // verticalAlignment: VerticalAlignment.Bottom
                        attachedObjects: [
                            ImagePaintDefinition {
                                id: actionbarpaint
                                imageSource: "asset:///images/nav_action_bar.png"
                            }
                        ]
                        layout: DockLayout {

                        }
                        Container {
                            minHeight: 125
                            minWidth: 150
                            gestureHandlers: TapHandler {
                                onTapped: {
                                    navPane.pop();

                                }
                            }

                        }

                    }
                }

            }
        },
        SystemToast {
            id: toastinter
            body: ""
            button.label: qsTr("Ok")
            button.enabled: true
        },
        ExternalIP {
            id: netip
            onComplete: {
                if (info == "Http Error: 0") {
                    console.log("No internet")

                    toastinter.body = "Check your Internet connection and try again."
                    toastinter.exec()

                    //customdialog.open()
                } else {

                    console.log("Yes internet")

                }

            }
        }
    //! [4]

    //! [5]
    // The dynamically loaded page to show an article in a webview
    //        ComponentDefinition {
    //            id: detailPage
    //            Page {
    //                property alias string : detailView.text
    //                Container {
    //                    ScrollView {
    //                        scrollViewProperties.scrollMode: ScrollMode.Vertical
    //                        Container {
    //
    //
    //                            WebView {
    //                                id: detailView
    //
    //                            }
    //
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //! [5]
    ]
}
