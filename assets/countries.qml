import bb.cascades 1.0
import QtQuick 1.0
import "js/parser.js" as JS

        Container {
            layout: DockLayout {
                
            }

	Container {
  		
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            layout: DockLayout {}
            
            ImageView {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill                        
                imageSource: "asset:///images/titlebarfin.png"
            }
            
            Label {
                horizontalAlignment: HorizontalAlignment.Center
                
                text: qsTr ("Choose category")
                textStyle.base: SystemDefaults.TextStyles.BigText
                textStyle.color: Color.White
            }
        }
        ScrollView {
        horizontalAlignment: horizontalAlignment.Fill    
        verticalAlignment: verticalAlignment.Fill
	  Container {
          layout: AbsoluteLayout {
          }
       ImageView {
           id: music
           layoutProperties: AbsoluteLayoutProperties {
               positionX: 20
               positionY: 20
           }
           preferredWidth: 728
           preferredHeight: 354
           imageSource: "asset:///images/MusicRadio.png"
           gestureHandlers: [
               TapHandler {
                   onTapped: {
               console.debug("Clicked")
               selectedItemCategory = "28"
               console.debug("selectedItemCategory", selectedItemCategory)
               navPane.push(nextPage.createObject());     

           }
               }
           ]
       }   
       Label {
           text: qsTr("Music")
           layoutProperties: AbsoluteLayoutProperties {
               positionX: 350
               positionY: 25
           }
           textStyle {
               base: SystemDefaults.TextStyles.TitleText
               color: Color.White
           }
       }
		   ImageView {
		       id: quran
               layoutProperties: AbsoluteLayoutProperties {
                   positionX: 20
                   positionY: 394
               }
		       preferredWidth: 728
               preferredHeight: 354
               imageSource: "asset:///images/Quran.png"
               gestureHandlers: [
                   TapHandler {
                       onTapped: {
                   selectedItemCategory = "29"
		           navPane.push(nextPage.createObject());
		           
		       }
                   }]
		   }
           Label {
               text: qsTr("Koran")
               layoutProperties: AbsoluteLayoutProperties {
                   positionX: 345
                   positionY: 404
               }
               textStyle {
                   base: SystemDefaults.TextStyles.TitleText
                   color: Color.White
               }
           }
           ImageView {
               id: varient
               layoutProperties: AbsoluteLayoutProperties {
                   positionX: 20
                   positionY: 768
               }
               preferredWidth: 728
               preferredHeight: 354
               imageSource: "asset:///images/variousMusic.png"
               gestureHandlers: [
                   TapHandler {
                       onTapped: {
                           selectedItemCategory = "30"
                           navPane.push(nextPage.createObject());
                       }
                   }]
           }
           Label {
               text: qsTr("Various")
               layoutProperties: AbsoluteLayoutProperties {
                   positionX: 320
                   positionY: 765
               }
               textStyle {
                   base: SystemDefaults.TextStyles.TitleText
                   color: Color.White
               }
           }
  }		
}   		   

        }
}
   