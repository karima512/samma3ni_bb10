import bb.cascades 1.0
import "js/parser.js" as JS

Container {
    background: tabpaint.imagePaint
    verticalAlignment: VerticalAlignment.Fill
    minWidth: 570.0
    attachedObjects: [
        ImagePaintDefinition {
            id: tabpaint
            imageSource: "asset:///images/tab_bg.png"
        }
    ]
    Container {
        layout: DockLayout {
        
        }
        ImageView {
            imageSource: "asset:///images/tab_title.png"
        }
        Label {
            text: "Samma3ni"
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            textStyle.color: Color.White
            textStyle.fontSize: FontSize.XXLarge
        }
    }
    ScrollView {
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        Container {
            
            TabbedItem {
                id: tab1
               // tabTitle: "Radios"
                tabImage: "asset:///images/selected_tab_radio.png"
                tabIcon: "asset:///images/1tab.png"
                gestureHandlers: TapHandler {
                    onTapped: {
                        tab1.tabImage = "asset:///images/selected_tab_radio.png"
                        tab2.tabImage = "asset:///images/unselected_tab_news.png"
                        tab3.tabImage = "asset:///images/unselected_tab_favorites.png"
                        tab4.tabImage = "asset:///images/unselected_tab_search.png"
                        tab5.tabImage = "asset:///images/unselected_tab_recording.png"
                        ab_selected_img.imageSource = "asset:///images/1tab.png"
                        ab_selected_img.visible = true;
                        ab_bg_img.imageSource = "asset:///images/action_bg.png"
                        
                        tabDelegate.source = "countries.qml"
                        contentCont.translationX = 0;
                        startPosition = 0;
                        finalPosition = 0;
                        fadeContainer.opacity = (contentCont.translationX/1000)*0.5;
                    }
                    
                }
                
            }
            
            TabbedItem {
                id: tab2
               // tabTitle: "News" 
                tabImage: "asset:///images/unselected_tab_news.png"
                tabIcon: "asset:///images/2tab.png"
                gestureHandlers: TapHandler {
                    onTapped: {
                        tab1.tabImage = "asset:///images/unselected_tab_radio.png"
                        tab2.tabImage = "asset:///images/selected_tab_news.png"
                        tab3.tabImage = "asset:///images/unselected_tab_favorites.png"
                        tab4.tabImage = "asset:///images/unselected_tab_search.png"
                        tab5.tabImage = "asset:///images/unselected_tab_recording.png"
                        ab_selected_img.imageSource = "asset:///images/2tab.png"
                        ab_selected_img.visible = true;
                        ab_bg_img.imageSource = "asset:///images/action_bg.png"
                        tabDelegate.source = "newscategories.qml"
                        contentCont.translationX = 0;
                        startPosition = 0;
                        finalPosition = 0;
                        fadeContainer.opacity = (contentCont.translationX/1000)*0.5;
                    }
                }
            
            }
            TabbedItem {
                id: tab3
              //  tabTitle: "Favorites" 
                tabImage: "asset:///images/unselected_tab_favorites.png"
                tabIcon: "asset:///images/3tab.png"
                gestureHandlers: TapHandler {
                    onTapped: {
                        tab1.tabImage = "asset:///images/unselected_tab_radio.png"
                        tab2.tabImage = "asset:///images/unselected_tab_news.png"
                        tab3.tabImage = "asset:///images/selected_tab_favorite.png"
                        tab4.tabImage = "asset:///images/unselected_tab_search.png"
                        tab5.tabImage = "asset:///images/unselected_tab_recording.png"
                        ab_selected_img.imageSource = "asset:///images/3tab.png"
                        ab_selected_img.visible = true;
                        ab_bg_img.imageSource = "asset:///images/action_bg.png"
                        
                        tabDelegate.source = "favoris.qml"
                        contentCont.translationX = 0;
                        startPosition = 0;
                        finalPosition = 0;
                        fadeContainer.opacity = (contentCont.translationX/1000)*0.5;
                    }
                }
            }
            TabbedItem {
                id: tab4
               // tabTitle: "Search radio" 
                tabImage: "asset:///images/unselected_tab_search.png"
                tabIcon: "asset:///images/4tab.png"
                gestureHandlers: TapHandler {
                    onTapped: {
                        tab1.tabImage = "asset:///images/unselected_tab_radio.png"
                        tab2.tabImage = "asset:///images/unselected_tab_news.png"
                        tab3.tabImage = "asset:///images/unselected_tab_favorites.png"
                        tab4.tabImage = "asset:///images/selected_tab_search.png"
                        tab5.tabImage = "asset:///images/unselected_tab_recording.png"
                        ab_selected_img.imageSource = "asset:///images/4tab.png"
                        ab_selected_img.visible = true;
                        ab_bg_img.imageSource = "asset:///images/action_bg.png"
                        tabDelegate.source = "searchradio.qml"
                        contentCont.translationX = 0;
                        startPosition = 0;
                        finalPosition = 0;
                        fadeContainer.opacity = (contentCont.translationX/1000)*0.5;
                    }
                }
            }
            TabbedItem {
                id: tab5
               // tabTitle: "Recording List" 
                tabImage: "asset:///images/unselected_tab_recording.png"
                tabIcon: "asset:///images/5tab.png"
                gestureHandlers: TapHandler {
                    onTapped: {
                        tab1.tabImage = "asset:///images/unselected_tab_radio.png"
                        tab2.tabImage = "asset:///images/unselected_tab_news.png"
                        tab3.tabImage = "asset:///images/unselected_tab_favorites.png"
                        tab4.tabImage = "asset:///images/unselected_tab_search.png"
                        tab5.tabImage = "asset:///images/selected_tab_recording.png"
                        ab_selected_img.imageSource = "asset:///images/5tab.png"
                        ab_selected_img.visible = true;
                        ab_bg_img.imageSource = "asset:///images/action_bg.png"
                        tabDelegate.source = "recordinglist.qml"
                        contentCont.translationX = 0;
                        startPosition = 0;
                        finalPosition = 0;
                        fadeContainer.opacity = (contentCont.translationX/1000)*0.5;
                    }
                }
            }
        }
    }

}
