import bb.cascades 1.0
import bb.cascades.pickers 1.0
import bb.data 1.0

Page {
    id: infoPage
    property string mailto : ""

    // Custom signal for notifying that this page needs to be closed
    signal done ()
    
    titleBar: TitleBar {
        title: qsTr("           About")
        dismissAction: ActionItem {
            title: qsTr("Back")
            //imageSource: "images/ic_previous.png"
            onTriggered: {
                 infoPage.done();
            }
        }
    }
    
    Container {
        background: back.imagePaint
        attachedObjects: [
            ImagePaintDefinition {
                id: back
                repeatPattern: RepeatPattern.XY
                imageSource: "asset:///images/aboutkarima.png"
            }]
                
        layout: DockLayout {
        }
        Container {
            topPadding: 500
            preferredWidth: 700
            bottomPadding: 30
            rightPadding: 20
            leftPadding: 20
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
          //  background: Color.create("#9960A917")
            
            layout: StackLayout {
            }
          
            Label {
                text: qsTr("Samma3ni is a radio hub for the gulf countries developed by :")
                multiline: true
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.fontSize: FontSize.Large
                textStyle.textAlign: TextAlign.Justify
                textStyle.color: Color.White
            }
            
            Container {
                preferredWidth: 1200
                leftPadding: 30
                rightPadding: 30
                horizontalAlignment: HorizontalAlignment.Fill
                layout: DockLayout {
                
                }
                Label {
                    text: "Karima MZOUGHI"
                    horizontalAlignment: HorizontalAlignment.Left
                    textStyle.fontSize: FontSize.Large
                    textStyle.textAlign: TextAlign.Justify
                    
                }
            
            }
            
            Label {
                text: qsTr("Email :")
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.fontSize: FontSize.XXLarge
                textStyle.textAlign: TextAlign.Justify
                textStyle.color: Color.White
            }
            
            Container {
                preferredWidth: 1200
                leftPadding: 30
                rightPadding: 30
                horizontalAlignment: HorizontalAlignment.Fill
                layout: DockLayout {
                }
                
                Label {
                   
                    text: "karima.mzoughi@esprit.tn"
                    
                    multiline: true
                    horizontalAlignment: HorizontalAlignment.Left
                    textStyle.fontSize: FontSize.Large
                    textStyle.textAlign: TextAlign.Justify   
                    textFormat: TextFormat.Html
                    onTouch: {
                        mailto = "karima.mzoughi@esprit.tn"
                        
                        if (event.isUp()){
                            invoke.trigger("bb.action.SENDEMAIL")
                        }   
                    }
                  
                }
            
            }
            

            attachedObjects: [
                Invocation {
		            id: invoke
		            query {
		                InvokeQuery {
                      id: invq                 
		            	mimeType: ""
		                invokeTargetId: "sys.pim.uib.email.hybridcomposer"
		                uri: "mailto:"+mailto+"?subject=Contact%20Us"
		                invokerIncluded: true
		                onQueryChanged: invq.updateQuery()          
		             }
		          }
		        }
            ]

        } 
      
    } 
    
} 
