import bb.cascades 1.0
import Network.ExternalIP 1.0
import bb.system 1.0
Container {
    property alias title : titleLabel.text
    property alias pubDate : pubDateLabel.text

    layout: DockLayout {}

    ImageView {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill

        imageSource: "asset:///icones/backgroundCell.png"
    }

    Container {
        horizontalAlignment: HorizontalAlignment.Fill
      //  verticalAlignment: VerticalAlignment.Fill
        preferredHeight: 90

        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }

        Container {
            topPadding: 50
            leftPadding: 50

            Label {
                id: titleLabel
                multiline: true
                textStyle.base: SystemDefaults.TextStyles.BodyText
                textStyle.color: Color.Black
            }

            Label {
                topPadding: 20
                id: pubDateLabel

                textStyle.base: SystemDefaults.TextStyles.SmallText
                textStyle.color: Color.Gray
            }
        }
//        Container {
//            ImageView {
//                verticalAlignment: VerticalAlignment.Center
//                minWidth: 20
//                topPadding: 50
//                leftPadding: 730
//                imageSource: "asset:///images/arrow.png"
//            }
//        }

       
    }
    attachedObjects: [
        
        SystemToast {
            id: toastinter
            body: ""
            button.label: qsTr("Ok")
            button.enabled: true
        },
        ExternalIP {
            id: netip
            onComplete: {
                if (info == "Http Error: 0") {
                    console.log("No internet")
                    
                    toastinter.body = "Check your Internet connection and try again."
                    toastinter.exec()
                    
                    //customdialog.open()
                } else {
                    
                    console.log("Yes internet")
                
                }
            
            }
        }
    
    
    ]
    onCreationCompleted: {
        netip.getIP()
    }
}
