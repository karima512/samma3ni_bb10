function load() {
	theDataModel.clear();
	var http = new XMLHttpRequest();
	var url = "http://radiogulf.hostei.com/scripts/getCountries.php";
	var params = "Token=radiogulf2014";
	http.open("POST", url, true);

	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http.setRequestHeader("Content-length", params.length);
	http.setRequestHeader("Connection", "close");

	http.onreadystatechange = function() {
		if (http.readyState == 4) {
			if (http.status == 200) {
				var jsonObject = eval('(' + http.responseText + ')');
				loaded(jsonObject);
			} else {
				console.log("error: " + http.status);
			}
		}
	};
	http.send(params);
}

function loaded(jsonObject) {
	for ( var index in jsonObject.categories) {
		theDataModel.append({
			"id" : jsonObject.categories[index]['ID'],
			"country" : jsonObject.categories[index]['country'],
			"flag" : "http://radiogulf.hostei.com/files/"
					+ jsonObject.categories[index]['flag']
		});
	}
}