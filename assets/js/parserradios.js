function load(Category) {
	dataModel.clear();
	var http = new XMLHttpRequest();
	var url = "http://radiogulf.hostei.com/scripts/getListRadio.php";
	var params = "Token=radiogulf2014&Category=" + Category;
	http.open("POST", url, true);

	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http.setRequestHeader("Content-length", params.length);
	http.setRequestHeader("Connection", "close");

	http.onreadystatechange = function() {
		if (http.readyState == 4) {
			if (http.status == 200) {
				var jsonObject = eval('(' + http.responseText + ')');
				loaded(jsonObject);
				// Afficher le r�sultat du Webservice
				console.log(http.responseText);
			} else {
				console.log("error: " + http.status);
			}
		}
	};
	http.send(params);
}

function loaded(jsonObject) {
	for ( var index in jsonObject.radios) {
		dataModel.append({
			"id" : jsonObject.radios[index]['ID'],
			"radio" : jsonObject.radios[index]['radio'],
			"logo" : "http://radiogulf.hostei.com/files/"
					+ jsonObject.radios[index]['logo'],
			"frequence" : jsonObject.radios[index]['frequence'],
			"urlradio" : jsonObject.radios[index]['urlradio']
		});
	}
}