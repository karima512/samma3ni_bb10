import bb.cascades 1.0

//! [0]
Container {
    
    property bool running: false

    onRunningChanged: {
        if (running)
            animation.play()
        else
            animation.stop()
    }

    layout: DockLayout {}

    ImageView {
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center

        imageSource: "asset:///images/tape_shadow.png"
    }

    ImageView {
        id: leftGear

        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center

        translationX: -155
        translationY: -8

        imageSource: "asset:///images/tape_gear.png"
    }

    ImageView {
        id: rightGear

        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center

        translationX: 138
        translationY: -8

        imageSource: "asset:///images/tape_gear.png"
    }

    ImageView {
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center

        imageSource: "asset:///images/tape_cover.png"
    }

    animations: ParallelAnimation {
        id: animation

        SequentialAnimation {
            target: leftGear

            RotateTransition {
                fromAngleZ: 0
                toAngleZ: 1800
                duration: 15000
                easingCurve: StockCurve.Linear
            }
        }
        SequentialAnimation {
            target: rightGear

            RotateTransition {
                fromAngleZ: 0
                toAngleZ: 1800
                duration: 15000
                easingCurve: StockCurve.Linear
            }
        }
    }
//! [0]
}
